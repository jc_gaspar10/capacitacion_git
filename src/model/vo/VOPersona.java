package model.vo;

public class VOPersona {


	private String nombre;
	private int edad;
	private String apellido;
	private long cedula;
	private VOPersona padre;
	private int dinero;
	// Se modifica
	//Nuevamente se modifica 
	//Aca ya no va 1
	//Va 2
	//aca va 2

	public VOPersona(String nombre, int edad, String apellido, long cedula, VOPersona padre, int dinero) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.apellido = apellido;
		this.cedula = cedula;
		this.padre = padre;
		this.dinero = dinero;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public long getCedula() {
		return cedula;
	}
	public void setCedula(long cedula) {
		this.cedula = cedula;
	}
	public boolean hasPadre()
	{
		return padre==null?false: true;
	}
	public VOPersona getPadre() {
		return padre;
	}
	public void setPadre(VOPersona padre) {
		this.padre = padre;
	}
	public int getDinero() {
		return dinero;
	}
	public void setDinero(int dinero) {
		this.dinero = dinero;
	}
	
	public int agregarDinero(int nDinero)
	{
		dinero+=nDinero;
		return dinero;
	}

}
