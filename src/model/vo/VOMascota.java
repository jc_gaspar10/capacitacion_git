package model.vo;

public class VOMascota {

	private String nombre;
	private VOPersona dueno;
	//se modifica mascotas
	//se modifica nuevamente
	public VOMascota(String nombre, VOPersona dueno) {
		super();
		this.nombre = nombre;
		this.dueno = dueno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public VOPersona getDueno() {
		return dueno;
	}

	public void setDueno(VOPersona dueno) {
		this.dueno = dueno;
	}
	
	
	
}
