package model.logic;

import java.util.ArrayList;

import model.vo.VOPersona;

public class PrincipalPrueba {

	private ArrayList<VOPersona> personas;
	private ArrayList<VOPersona> empleados;

	public PrincipalPrueba(ArrayList<VOPersona> personas, ArrayList<VOPersona> empleados) {
		super();
		this.personas = personas;
		this.empleados = empleados;
	}

	public ArrayList<VOPersona> getPersonas() {
		return personas;
	}

	public void setPersonas(ArrayList<VOPersona> personas) {
		this.personas = personas;
	}

	public ArrayList<VOPersona> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(ArrayList<VOPersona> empleados) {
		this.empleados = empleados;
	}

	public boolean agregarPersona(VOPersona persona)
	{
		return this.personas.add(persona);
	}

	public int numPersonas()
	{
		return this.personas.size();
	}

	public int numEmpleados()
	{
		return this.empleados.size();
	}

	public boolean agregarEmpleado(VOPersona empleado)
	{
		return this.empleados.add(empleado);
	}
}
